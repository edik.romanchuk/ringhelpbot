from telegram import Update
from telegram.ext import Updater, CommandHandler, CallbackContext
import random

TOKEN = '7450558480:AAEGOj9wUoO1ViehRoAPIxk78lzwW39vj6A'


def delete_after_delay(context: CallbackContext) -> None:
    job = context.job
    chat_id = job.context['chat_id']
    message_ids = job.context['message_ids']

    for message_id in message_ids:
        try:
            context.bot.delete_message(chat_id=chat_id, message_id=message_id)
        except Exception as e:
            print(f"Error deleting message {message_id}: {e}")


def faq(update: Update, context: CallbackContext) -> None:
    user_message_id = update.message.message_id
    bot_message = update.message.reply_text(
        "Волшебная статья, которая отвечает на большинство твоих вопросов.\n"
        "Если после прочтения что-то будет непонятно — спрашивай, с радостью поможем.\n\n"
        "О проекте: \n\n"
        "<a href='https://ringgame.co'>➡️ Ringgame.co</a>\n"
        "<a href='https://getgems.io/collection/EQBvpaLSzGnyp1-KKOR_6ZsiHnSBMgXA7DNe6cS00IVIKViq'>🔝 NFT collection RING</a>\n"
        "<a href='https://t.me/ringgame/2407'>💰 AIRDROP</a>\n"
        "<a href='https://t.me/+7BIozh1qh71mZTU0'>➡️ Наш дружный чат</a>\n"
        "<a href='https://t.me/ringadmi'>😕 Admin</a>\n"
        "<a href='https://t.me/boost/ringgame'>Проголосовать за RING</a>\n\n"
        "Ссылка для друзей: @ringgame\n\n"
        "Собрали для вас навигацию по важным статьям:\n\n"
        "1. <a href='https://t.me/ringgame/2342'>Кто мы?</a>\n"
        "2. <a href='https://t.me/ringgame/2392'>Планы нашего проекта</a>\n"
        "3. <a href='https://t.me/ringgame/2390'>Игра «Турнирный кликер»</a>\n"
        "4. <a href='https://t.me/ringgame/2379'>Ответ на ваши вопросы (1)</a>\n"
        "5. <a href='https://t.me/ringgame/2381'>Ответ на ваши вопросы (2)</a>\n"
        "6. <a href='https://t.me/ringgame/2396'>Ответ на ваши вопросы (3)</a>\n"
        "7. <a href='https://t.me/ringgame/2437'>Ответы на ваши вопросы (4)</a>\n"
        "8. <a href='https://t.me/ringgame/2375'>Как вас бреют 🐹 🪒</a>\n"
        "9. <a href='https://t.me/ringgame/2439'>Процесс создания Бэна</a>\n"
        "10. <a href='https://t.me/ringgame/2421'>Наши персонажи:</a>\n"
        "11. <a href='https://t.me/ringgame/2397'>Почему web3</a>\n"
        "12. <a href='https://t.me/ringgame/2439'>Что ждет нас и TON</a>\n\n"
        "@ringgame",
        parse_mode='HTML')
    context.job_queue.run_once(delete_after_delay,
                               20,
                               context={
                                   'chat_id':
                                   update.message.chat_id,
                                   'message_ids':
                                   [user_message_id, bot_message.message_id]
                               })


def kak(update: Update, context: CallbackContext) -> None:
    user_message_id = update.message.message_id
    bot_message = update.message.reply_text(
        "👉 <a href='https://t.me/ringgame/2407'>Запускаем AIRDROP</a>\n\n"
        "Вы сможете получить мем-токены $BEN и билеты на турнир с призами более 20.000$ просто приглашая друзей\n\n"
        "Условия — @ringairdropbot\n\n"
        "Будет проведена серьезная проверка на наличие ботов и мульти-аккаунтов. Все токены человека, который их привел будут сожжены 🔥\n\n"
        "CLAIM токенов будет доступен 03.06.2024\n\n"
        "Действуйте честно и все будет хорошо. Кто приводил ботов и мультиаки - не получит ничего\n\n"
        "Наш чат 💬 - <a href='https://t.me/+92Y8qSH6g_FlNTY0'>доступ</a>\n",
        parse_mode='HTML')
    context.job_queue.run_once(delete_after_delay,
                               20,
                               context={
                                   'chat_id':
                                   update.message.chat_id,
                                   'message_ids':
                                   [user_message_id, bot_message.message_id]
                               })

def money(update: Update, context: CallbackContext) -> None:
    user_message_id = update.message.message_id
    random_amount = random.randint(150, 2990)
    bot_message = update.message.reply_text(f"Ваш баланс: {random_amount}$")
    context.job_queue.run_once(delete_after_delay,
                               20,
                               context={
                                   'chat_id':
                                   update.message.chat_id,
                                   'message_ids':
                                   [user_message_id, bot_message.message_id]
                               })


def drop(update: Update, context: CallbackContext) -> None:
    user_message_id = update.message.message_id
    sticker_id = "CAACAgIAAxkBAAEF0CNmWPXmi3sJ_i2RgrQBJSk5qFvi-QACJwADr8ZRGpVmnh4Ye-0RNQQ"
    message_ids = [user_message_id]

    for _ in range(1):
        message = update.message.reply_sticker(sticker_id)
        message_ids.append(message.message_id)

    context.job_queue.run_once(delete_after_delay,
                               20,
                               context={
                                   'chat_id': update.message.chat_id,
                                   'message_ids': message_ids
                               })


def main() -> None:

    updater = Updater(TOKEN)

    dispatcher = updater.dispatcher

    dispatcher.add_handler(CommandHandler("faq", faq))
    dispatcher.add_handler(CommandHandler("kak", kak))
    dispatcher.add_handler(CommandHandler("money", money))
    dispatcher.add_handler(CommandHandler("drop", drop))

    updater.start_polling()

    updater.idle()


if __name__ == '__main__':
    main()
